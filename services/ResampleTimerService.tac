"""
I am a service that sends clock messages
to keep up with the time

I am going to talk to pubsub redis

"""

import time
import os
import sys

import json

from datetime import datetime
from pprint import pprint as pp

sys.path.append('/home/james/Seer/seer/clients/')


from twisted.internet import defer
from twisted.internet.task import react
from twisted.application import service
from twisted.application.internet import TimerService
from twisted.internet import reactor, protocol, defer
from twisted.python import log


import txredisapi as redis

#import redis as old_redis

REDIS_HOST='localhost'
REDIS_PORT=6379
REDIS_PUBSUB_PORT=6380


#fix me
#client = old_redis.StrictRedis(REDIS_HOST, REDIS_PORT)

name="Resampling Time Service"
name=name.lower().replace(' ', '_')

intervals=[15,30]
mins=[1,2,5,10,15,30,45,60]
hours=[2,4,8] # we have 60 min = 1 hr above

intervals.extend([aMin*60 for aMin in mins])
intervals.extend([hr*60*60 for hr in hours])

def craftMessage():
    out=[]
    now=datetime.now()
    log.msg(now)
    seconds=now.second
    minutes=now.minute

    total_seconds=minutes*60+seconds
    for interval in intervals:
        val=total_seconds%interval
        if val==0:
            put=1
        else:
            put=0
        out.append((interval,put))
    message={now.isoformat():dict(out) }
    #pub(json.dumps(message) )
    log.msg(json.dumps(message) )
    return dict(message)

@defer.inlineCallbacks
def pub(message):
    """send data to publishing redis
    """
    channel='%s.%s'%(name, os.getpid())
    db = yield redis.Connection(REDIS_HOST, REDIS_PUBSUB_PORT, reconnect=False)

    yield db.publish(channel,message)
    yield db.disconnect()

def tired_task():
    print "I want to run slowly", datetime.now()

log.msg('waiting for next interval')
while time.time()%15!=0:
    pass

application = service.Application(name)

#tired = TimerService(5, tired_task)
#tired.setServiceParent(application)

clock= TimerService(15, craftMessage)
clock.setServiceParent(application)



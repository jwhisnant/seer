"""
I am a service that gets market extended
quotes over some interval
I write the data out to redis
"""
from datetime import datetime
import time
from pprint import pprint as pp
import os
import json
import sys
from twisted.internet import defer

from twisted.internet.task import react

# fix blargh
sys.path.append('/home/james/Seer/seer/clients/')
sys.path.append('/home/james/Seer/seer/clients/quotes/')


from twisted.application import service
from twisted.application.internet import TimerService

from keys import consumer_key
from keys import consumer_secret
from keys import access_secret
from keys import access_token

from tradeking import TradeKingAPI
import txredisapi as redis
import redis as old_redis

from twisted.internet import reactor, protocol, defer
from twisted.python import log

REDIS_HOST='localhost'
REDIS_PORT=6379
REDIS_PUBSUB_PORT=6380



#fix me
client = old_redis.StrictRedis(REDIS_HOST, REDIS_PORT)

api=TradeKingAPI(CK=consumer_key,
                 CS=consumer_secret,
                 OT=access_token,
                 OS=access_secret,
                 response_format='xml')

api.name="TK Extended Quote Application"
api.name=api.name.lower().replace(' ', '_')

class CustomTimerService(TimerService):

    def changeInterval(self,interval):
        """we need to be able to change the interval
        """
        # not sure if this does anything
        if interval<0:
            interval=0

        self.step=interval
        # ick
        self._loop.interval=interval
        log.msg('Interval changed to %s'%(interval))

def getQuotes(**kwargs):
    """kwargs
    symbols - comma separated list of symbols
    fids - fids to get (not sure format yet)
    """
    # check to see if interval has changed ..

    # check to see if there are new symbols

    # check to see if the fids have changed

    #timerId=kwargs.get('tsid', '') or '' # not implemented

    symbols=kwargs.get('symbols',None)
    if not symbols:
        raise
    fids=kwargs.get('fids', None) or None
    now=time.time()
    req=api.market_ext_quotes(symbols, fids)

    # we need to add our own time in here
    req.headers['_now']=now

    expire=req.headers.get('x-ratelimit-expire')
    expire=int(expire)

    if expire<now:
        expire=expire+60

    delta=expire-req.headers['_now']
    remaining=req.headers.get('x-ratelimit-remaining')
    remaining=int(remaining)

    req.headers['_delta']=delta
    req.headers['_interval']=delta/remaining

    #pp(req.headers)

    from thissux import StreamingQuotePublisher
    destination=StreamingQuotePublisher()
    destination.connectionMade() # do some required setup
    destination.dataReceived(req.text.encode('utf-8'))

    pub(req)
    cache(req)
    return defer.succeed(req)

@defer.inlineCallbacks
def pub(request):
    """send data to publishing redis
    """
    import ipdb;ipdb.set_trace()
    channel='%s.%s'%(api.name, os.getpid())
    db = yield redis.Connection(REDIS_HOST, REDIS_PUBSUB_PORT, reconnect=False)
    yield db.publish(channel,request.text)
    yield db.disconnect()

@defer.inlineCallbacks
def cache(request,tsid=''):
    """send data to persistent redis
    """
    import ipdb;ipdb.set_trace()
    # cache the data in cache we need it ...
    channel='%s.%s'%(api.name, os.getpid())

    db = yield redis.Connection(REDIS_HOST, REDIS_PORT, reconnect=False)
    yield db.rpush(channel,request.text)
    #cache_header_data(request,tsid)
    #pp(request.headers.items())
    yield db.disconnect()

@defer.inlineCallbacks
def cache_header_data(request,tsid=''):
    """send data to persistent redis
    """
    # cache the data in cache we need it ...
    channel='%s.%s'%(api.name, os.getpid())

    db = yield redis.Connection(REDIS_HOST, REDIS_PORT, reconnect=False)

    if not tsid:
        header_key=channel+'.header'

    if tsid:
        header_key='%s.%s.%s'%(channel,tsid,'.header')

    # we are going to inject _now since that is the best way to calculate time
    # since the server's date is bozotic

    headerDict=dict(request.headers.items())
    data=json.dumps(headerDict)

    yield db.rpush(header_key,data)
    yield db.disconnect()

def calc_new_interval(header, fudge=5):
    """
'x-ratelimit-expire': '1369604520'
'x-ratelimit-remaining': '59'
'x-ratelimit-used:'3'
"x-ratelimit-limit":60
"date":Sun, 26 May 2013 23:38:25 GMT # of course this is the time on their server, which is WRONG!

    fudge is a factor to make sure we dont run over
    there should be a better way of dealing with it
    """
    #import ipdb;ipdb.set_trace()
    out=None

    if not header:
        return out

    data=json.loads(header)

    #injected data, now
    now=data.get('_now')

    expire_epoch=data.get('x-ratelimit-expire', 0) or 0
    expire_time=int(expire_epoch)

    remaining=data.get('x-ratelimit-remaining', 0 ) or 0
    remaining=int(remaining)

    #used=data.get('x-ratelimit-used')
    #used=int(used)

    if not remaining: # no more calls
        return expire_time-now

    if expire_time and now and remaining:
        temp=expire_time-(now+fudge)
        if temp>0:
            try:
                out=temp/remaining
            except: #exceptions.ZeroDivisionError: float division by zero
                out=None

    return out

def check_timings(*args):
    """talk to redis - get stored headers, do math
    change my step
    """
    header=None
    for test in args:
        header_key='%s.%s.header'%(api.name, os.getpid())
        header=client.rpop(header_key)

    if header:
        new_int=calc_new_interval(header)

        if new_int is not None:
            for timerService in args:
                timerService.changeInterval(new_int)

#        client.delete(header_key)
    return args


def read_symbols():
    out=[]
    small=[]

    fn='/home/james/stocks/quant/symbols.txt'
    for number,line in enumerate(open(fn, 'r')):
        if number%1000==0:
            if small:
                fixed=','.join(small)
                out.append(fixed)
                small=[]
        small.append(line.strip())

    #leftovers
    fixed=','.join(small)
    out.append(fixed)
    return out

def tired_task():
    print "I want to run slowly", datetime.now()


symbols=read_symbols()

#kwargs={'symbols':'AAPL,IBM', 'fids':None}
#moekwargs={'symbols':'GOOG,VTI', 'fids':None}
kwargs={'symbols':symbols[0], 'fids':None}
moekwargs={'symbols':symbols[1], 'fids':None}


application = service.Application(api.name)

num_clients=1 # FIX ME
calls_per_second=1
interval=num_clients*calls_per_second
interval=1800 #XX FIX ME
ts = CustomTimerService(interval, getQuotes, **kwargs)
ts.setServiceParent(application)

#ts2 = CustomTimerService(interval, getQuotes, **moekwargs)
#ts2.setServiceParent(application)

#tired = TimerService(5, tired_task)
#tired.setServiceParent(application)

#check= TimerService(5,  check_timings, ts )
#check.setServiceParent(application)



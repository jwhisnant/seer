"""
I am a subscriber class that has
a dataframe object.  When data gets published to redis
I listen and then I update the customdataframe with
data



RUN ME WITH TWISTD!!!!

"""
import sys
sys.path.append('/home/james/Seer/seer/clients/quotes')

import json

import txredisapi as redis

from twisted.application import internet
from twisted.application import service
from twisted.python import log
from twisted.internet import defer
from twisted.internet import reactor

from message import Message
from customdataframe import CustomDataFrame

#protocols

class PublisherProtocol(redis.RedisProtocol):
    """ same old same old ...
    """

quote={u'2013-05-30T19:54:25-04:00': {u'ask': u'451.7',
  u'asksz': u'10',
  u'bid': u'451.6',
  u'bidsz': u'2',
  u'bidtick': u'3',
  u'symbol': u'AAPL'}}

trade={u'2013-05-30T19:54:25-04:00': {u'cvol': u'12629548',
  u'hi': u'35.24',
  u'last': u'451.69',
  u'symbol': u'AAPL',
  u'tcond': u'57,16,10',
  u'vl': u'100',
  u'vwap': u'449.4911'}}

old_quote= {'ask': '442.75',
      'asksz': '4',
      'bid': '442.54',
      'bidsz': '1',
      'bidtick': '3',
      'datetime': '2013-05-23T16:15:01-04:00',
      'exch': 'Pacific',
      'qcond': 'Regular, two-sided open quote automated',
      'symbol': 'AAPL',
      'timestamp': '1369340101'}

old_trade={'cvol': '12598621',
     'datetime': '2013-05-23T17:13:20-04:00',
     'exch': 'Pacific',
     'last': '442.88',
     'symbol': 'AAPL',
     'tcond': '57,16,10',
     'timestamp': '1369343600',
     'vl': '196',
     'vwap': '442.0288'}


class TestingPublisherProtocol(redis.RedisProtocol):
    """ a publishing protocol that sends some test messages
    """

    def connectionMade(self):
        log.msg("Connection made")
        self.sendStockMessages()

    def sendStockMessages(self):
        log.msg("Publishing messages")
        print "publishing messages"
        for aDict in (self.trade,self.quote):
            chan=aDict.get('symbol', 'UNK') + '.'+'quote'
            #self.publish(chan,aDict) # no need to do json
            #self.publish(chan,json.dumps(aDict.items()))

            out={aDict.get('datetime') : aDict.copy()}
            log.msg(out)
            self.publish(chan,json.dumps(out) )

class CustomSubscriberProtocol(redis.SubscriberProtocol):

    @defer.inlineCallbacks
    def publish(self,channel, message):
        db = yield redis.Connection('127.0.0.1', 6379, reconnect=False)
        yield db.publish(channel, message)
        yield db.disconnect()


    def sendGenericMessage(self,msg):
        """key and value same ...
        """
        key=self.messenger.create_outgoing_key(msg=msg)
        self.publish(key,msg)

    def initService(self):
        """send a start message
        with identifier
        """
        #request work ...
        self.sendGenericMessage('startup')

    def status(self,**kwargs):
        """when asked for a status, reply with something
        probably self.symbols and self.fids
        # return some values
        """
        out={'symbol':self.symbol,
            'df_len':len(self.framer.dataframe),
            }
        return out

    def shutdown(self):
        """when asked for a status, reply with something
        probably self.symbols and self.fids
        # return some values
        """
        self.sendGenericMessage('shutdown')
        reactor.callLater(1, reactor.stop)

    def connectionMade(self):
        """we subscribe to appropriate things on connecting
        to the redis server

        This includes the timing service as well as for any
        data about the symbol we are watching

        """
        # gimmme message subscriber
        self.symbol='AAPL'
        subs=[
            '%s.quote'%(symbol),
            '%s.trade'%(symbol),
            'Timer',

            # testing
            "*|*|control.*",
            ]

        self.messenger=Message("Quote Message Subsriber", subscriptions=subs)
        #aFactory.protocol.symbols='AAPL'
        self.framer=CustomDataFrame()

        for channel in self.messenger.subscriptions:
            self.psubscribe(channel)

        # ask for somebody to give me stuff to do ...
        self.initService()

    def handleQuote(self,channel,message):
        self.framer.dataReceived(message)

    def handleTrade(self,channel,message):
        self.framer.dataReceived(message)

    def handleControl(self,channel,message):
        """
        data=={'msg_type':msg_type,
            'src':src,
            'me':me,
            'method':method,
            }
        """

        #parse message
        data=self.messenger.decode_control_message(channel,message)
        my_data=self.callFromRemote(data)

        # wrap data up ...
        channel=self.messenger.create_outgoing_key(
            target=data.get('src'),
            msg=data.get('msg_type')+'.response')

        # send response
        self.publish(channel,my_data)

    def callFromRemote(self,data):
        method=data.get('method', None) or None
        kwargs=data.get('kwargs', {}) or {}
        kwargs={} #testing

        result=None

        if not method:
            data['result']=None
            data['error']='NoSuchMethod'
            return data

        if method:
            to_call=getattr(self,method, None)
            # unjsonify it
            if to_call:
                result=to_call(**kwargs)
        return result


    def messageReceived(self, pattern, channel, message):
        print "pattern=%s, channel=%s message=%s" % (pattern, channel, message)

        if not isinstance(message,int): # we get acks on subscriptions, message is an int
            if 'control' in channel:
                self.handleControl(channel,message)

            if 'trade' in channel:
                self.handleTrade(channel,message)

            if 'quote' in channel:
                self.handleQuote(channel,message)

            if 'emergency' in channel:
                import ipdb;ipdb.set_trace()

    def connectionLost(self, reason):
        print "lost connection:", reason

# factories
class PublisherFactory(redis.RedisFactory):
    # PublisherFactory is a wapper for the ReconnectingClientFactory
#    maxDelay = 120
#    continueTrying = True
    protocol = PublisherProtocol

    def __init__(self, isLazy=False, handler=redis.ConnectionHandler):
        redis.RedisFactory.__init__(self, None, None, 1, isLazy=isLazy,
            handler=handler)


class TestingPublisherFactory(redis.RedisFactory):
    # PublisherFactory is a wapper for the ReconnectingClientFactory
#    maxDelay = 120
#    continueTrying = True
    protocol = TestingPublisherProtocol

    def __init__(self, isLazy=False, handler=redis.ConnectionHandler):
        redis.RedisFactory.__init__(self, None, None, 1, isLazy=isLazy,
            handler=handler)


class CustomSubscriberFactory(redis.SubscriberFactory):
    # SubscriberFactory is a wapper for the ReconnectingClientFactory

    maxDelay = 120
    continueTrying = True
    protocol = CustomSubscriberProtocol
    #protocol = redis.RedisProtocol #no worky

name="Quote Message Subsriber"
symbol='AAPL'
application = service.Application(name)

#srv = internet.TCPClient("127.0.0.1", 6379, PublisherFactory())
#srv = internet.TCPClient("127.0.0.1", 6379, TestingPublisherFactory())
#import ipdb;ipdb.set_trace()

aFactory=CustomSubscriberFactory()
aFactory.protocol.name=name
# we is listening on the pubsub port
subscriberService = internet.TCPClient("127.0.0.1", 6379, aFactory )
subscriberService.setServiceParent(application)


#pubfactory=PublisherFactory()
#publisherService = internet.TCPClient("127.0.0.1", 6379, pubfactory )
#publisherService.setServiceParent(application)




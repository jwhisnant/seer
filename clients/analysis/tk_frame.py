"""
I am a DataFrame that is intended to
be a subscriber and publisher to redis
"""
import sys
import json
import pandasjson
from pandas import DataFrame
import pandas as pd
from pandas.stats.moments import ewma
from pprint import pprint as pp

import txredisapi as redis

from twisted.application import internet
from twisted.application import service

from twisted.python import log

from twisted.internet import task

class TkFrame(object):

    def __init__(self, symbols=None, frame=None, mychannels=None):
        """frame is going to be a dataframe
        and subscribe a list of things to subscribe to
        """
        self.frame=frame
        self.mychannels=mychannels
        self.symbols=symbols

        if isinstance(symbols, (str,unicode)):
            self.symbols=[]
            self.symbols.append(symbols)

        if not self.mychannels:
            self.mychannels=['broadcast.*', ]

        for sym in self.symbols:
            self.mychannels.append('%s.quote'%(sym))

class myProtocol(redis.SubscriberProtocol):

    messages_received=0
    frame=TkFrame(symbols=('AAPL',) )

    def connectionMade(self):
        print "waiting for messages... %s"%(','.join(self.frame.mychannels))
        self.psubscribe(self.frame.mychannels)

    def messageReceived(self, pattern, channel, message):
        #print "pattern=%s, channel=%s message=%s" % (pattern, channel, message)
        aFrame=None
        try:
            aFrame=DataFrame.from_json(message, orient='index')
            aFrame.index=pd.to_datetime(aFrame.index)
            self.messages_received+=1

        except:
            print "pattern=%s, channel=%s message=%s" % (pattern, channel, message)

        if aFrame is not None:
            if self.frame.frame is not None:

                foo=self.frame.frame.reset_index()
                bar=aFrame.reset_index()
                bob=pd.merge(foo,bar, how='outer')
                self.frame.frame=bob.set_index('index')
                self.frame.frame.sort_index(inplace=True)

            if self.frame.frame is None:
                self.frame.frame=aFrame



        if self.messages_received%10==0:
        #if self.messages_received: # XXX FIX ME
            pp(self.frame.frame.head())
            pp(self.frame.frame.tail())
            print len(self.frame.frame)

    def connectionLost(self, reason):
        print "lost connection:", reason


class myFactory(redis.SubscriberFactory):
    # SubscriberFactory is a wapper for the ReconnectingClientFactory
    maxDelay = 120
    continueTrying = True
    protocol = myProtocol

application = service.Application("subscriber")

aFactory=myFactory()
#aFactory.protocol._addFrame(symbols=('AAPL',) )

srv = internet.TCPClient("127.0.0.1", 6379, aFactory)
srv.setServiceParent(application)

'''
timeout=5
looping=task.LoopingCall(showLog, aFactory.protocol)
looping.start(timeout)
'''




'''
fn='/home/james/Seer/seer/clients/quotes/data/foo.json'


ohlc_dict = {
    'opn':'first',
    'hi':'max',
    'lo':'min',
    'last': 'last',
    'incr_vl': 'sum',
}

def clean(aDict):
    out={}
    for key,value in aDict.iteritems():
        try:
            value=float(value)
        except:
            value=value
        out[key]=value
    return out

bar=json.load(open(fn, 'r'))

#import ipdb;ipdb.set_trace()
temp=bar.get('response').get('quotes').get('quote')

one=[]
for aDict in temp:
    new=clean(aDict)
    one.append(new)

#bob=DataFrame(one, index=[x.get('datetime') for x in one] )
to_exclude=['datetime', 'date', 'timestamp', 'time', 'vl']

#vl is supposed to be cum vol, but it is wrong ...
#[u'last', u'timestamp', u'time', u'vl', u'datetime', u'date', u'hi', u'lo', u'incr_vl', u'opn']


foo=DataFrame.from_records(one, index='datetime',
    exclude=to_exclude)
#    coerce_float=True)
foo.index=pd.to_datetime(foo.index)

import ipdb;ipdb.set_trace()

aSeries=foo['last']
foo['ewma_20']=ewma(aSeries,span=20,min_periods=20)
foo['ewma_8']=ewma(aSeries,span=8,min_periods=8)



newdata=[]
five=DataFrame()

newDf=foo.resample('5Min', how=ohlc_dict, label='left', closed='left')

print 'bar'
'''

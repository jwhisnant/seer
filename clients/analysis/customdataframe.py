#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: customdataframe.py
Author: James Whisnant
Email: jwhisnant@gmail.com
Github: https://github.com/jwhisnant
Description:

Create a custom dataframe that makes some of the common opeations easier

1) ewma
2) combine new data
3) pandas json supporting

do analysis
ewma
macd
parabolic SAR
price at volume

3) resampling data
"""

import sys
import json

from pandas import DataFrame
# import pandasjson

import pandas as pd

from pandas.stats.moments import ewma
from pprint import pprint as pp

import talib

from numpy import sign

'''
import txredisapi as redis
from twisted.application import internet
from twisted.application import service
from twisted.python import log
from twisted.internet import task
'''


class CustomDataFrame(object):

    def __init__(self, dataframe=None, samples=None, data=None):
        """frame is going to be a dataframe
        and subscribe a list of things to subscribe to

        dataframe is a DataFrame
        samples are so storing resampled dataframes
        data is a message that is json, from redis more than likely
        """
        self.dataframe = dataframe
        self.samples = samples
        self.data = data

        if not self.samples:
            self.samples = {}

        if not self.dataframe:
            self.dataframe = DataFrame()

    def _testing_call(self, data=None):  # TESTING
        """
        data is some data that needs to be added
        """
        # testing stuff
        from pandas.io.data import DataReader
        from datetime import datetime

        self.dataframe = DataReader("AAPL", "yahoo")

        '''

Open         854  non-null values
High         854  non-null values
Low          854  non-null values
Close        854  non-null values
Volume       854  non-null values
Adj Close    854  non-null values
        '''

        ohlc_dict = {
            'Open': 'first',
            'High': 'max',
            'Low': 'min',
            'Volume': 'sum',
        }

        # self._resample('5Min')
        self._resample('1W', how=ohlc_dict)
        # ewmadata=[('Adj Close', 8), ('Adj Close', 20)]
        # self._ewma(ewmadata)

        # do avg price before ewma
        series = [self.dataframe.get('High'), self.dataframe.get('Low')]
        self._avgprice(series=series)
        self._ewma()

        # test data
        kwargs = {'high': self.dataframe['High'].as_matrix(), 'low': self.dataframe['Low'].as_matrix()}
        self.sarext(**kwargs)
        # self.sar(**kwargs) # this not appear helpful
        return self

    def dataReceived(self, data):
        """
        what to do if we get some sort of data
        """
        self._addJsonDict(data)

    def __call__(self, data=None):
        """
        data is some data that needs to be added
        """
        # self._addJsonDict(data)
        # self._resample('5Min')
        self._ewma()

        # tk data
        kwargs = {'high': self.dataframe['hi'].as_matrix(), 'low': self.dataframe['lo'].as_matrix()}
        self.sarext(**kwargs)

        return self

    def sarext(self, **kwargs):
        """use TAlib to calculate parabolic SAR
        startvalue: 0
        offsetonreverse: 0
        accelerationinitlong: 0.02
        accelerationlong: 0.02
        accelerationmaxlong: 0.2
        accelerationinitshort: 0.02
        accelerationshort: 0.02
        accelerationmaxshort: 0.2
        """

        startvalue = kwargs.get('startvalue') or 0
        offsetonreverse = kwargs.get('offsetonreverse') or 0
        accelerationinitlong = kwargs.get('accelerationinitlong') or 0.02
        accelerationlong = kwargs.get('accelerationlong') or 0.02
        accelerationmaxlong = kwargs.get('accelerationmaxlong') or 0.2
        accelerationinitshort = kwargs.get('accelerationinitshort') or 0.02
        accelerationshort = kwargs.get('accelerationshort') or 0.02
        accelerationmaxshort = kwargs.get('accelerationmaxshort') or 0.2

        # a dataframe, default to self
        frame = kwargs.get('frame') or self.dataframe

        high = low = None

        high = kwargs.get('high', None)
        low = kwargs.get('low', None)

        if high is None:
            high = frame['hi']

        if low is None:
            low = frame['lo']

        result = talib.SAREXT(high, low,
                              startvalue=startvalue,
                              offsetonreverse=offsetonreverse,
                              accelerationinitlong=accelerationinitlong,
                              accelerationlong=accelerationlong,
                              accelerationmaxlong=accelerationmaxlong,
                              accelerationinitshort=accelerationinitshort,
                              accelerationshort=accelerationshort,
                              accelerationmaxshort=accelerationmaxshort,
                              )

        frame['sarext_sign'] = result
        frame['sarext'] = abs(frame['sarext_sign'])
        frame['sarext_sign'] = frame['sarext_sign'].apply(sign)

        return frame

    def sar(self, **kwargs):
        """use TAlib to calculate parabolic SAR
        startvalue: 0
        offsetonreverse: 0
        accelerationinitlong: 0.02
        accelerationlong: 0.02
        accelerationmaxlong: 0.2
        accelerationinitshort: 0.02
        accelerationshort: 0.02
        accelerationmaxshort: 0.2
        """

        maximum = kwargs.get('maximum') or 0.02
        acceleration = kwargs.get('acceleration') or 0.20

        # a dataframe, default to self
        frame = kwargs.get('frame') or self.dataframe

        high = low = None

        high = kwargs.get('high', None)
        low = kwargs.get('low', None)

        if high is None:
            high = frame['hi']

        if low is None:
            low = frame['lo']

        result = talib.SAR(high, low,
                           maximum=maximum,
                           acceleration=acceleration,
                           )

        frame['sar'] = result
        return frame

    def _addJsonDict(self, message, frame=None):
        """message should be a json message that is
        a dictionary that is probably from redis
        """
        aFrame = DataFrame.from_json(message, orient='index')
        aFrame.index = pd.to_datetime(aFrame.index)

        if frame is None:
            frame = self.dataframe

        # if frame is not None:
        foo = frame.reset_index()
        bar = aFrame.reset_index()
        bob = pd.merge(foo, bar, how='outer')
        frame = bob.set_index('index')
        frame.sort_index(inplace=True)

        self.dataframe = frame

        return frame

    def _avgprice(self, series=None, frame=None):
        if frame is None:
            frame = self.dataframe
        if not series:
            series = [frame.get('high'), frame.get('lo')]
        avg = sum(series) / len(series)
        frame['avg_price'] = avg
        return frame

    def _resample(self, interval='5Min', how=None, label='left', closed='left', frame=None):
        """called when we are ordered to resample data
        """
        if frame is None:
            frame = self.dataframe

        if not how:
            how = {
                'opn': 'first',
                'hi': 'max',
                'lo': 'min',
                'last': 'last',
                'incr_vl': 'sum',
            }

        newDf = frame.resample(interval, how=how, label=label, closed=closed)
        newDf.abs()
        self.samples[interval] = newDf
        return newDf

    def _ewma(self, data=None, frame=None):
        """
        intervals is a list to calculate
        frame is a custom frame if we need that instead
        data is list of (colname,interval)
        """
        if frame is None:
            frame = self.dataframe

        if not data:
            data = [('avg_price', 8), ('avg_price', 20)]

        for colname, interval in data:
            aSeries = frame[colname]
            frame['ewma_%s_%s' % (colname, interval)] = ewma(aSeries, span=interval, min_periods=interval)
        return frame


if __name__ == '__main__':
    df = CustomDataFrame()
    stuff = df._testing_call()
    # stuff=df()

'''
timeout=5
looping=task.LoopingCall(showLog, aFactory.protocol)
looping.start(timeout)
'''


'''
fn='/home/james/Seer/seer/clients/quotes/data/foo.json'


ohlc_dict = {
    'opn':'first',
    'hi':'max',
    'lo':'min',
    'last': 'last',
    'incr_vl': 'sum',
}

def clean(aDict):
    out={}
    for key,value in aDict.iteritems():
        try:
            value=float(value)
        except:
            value=value
        out[key]=value
    return out

bar=json.load(open(fn, 'r'))

#import ipdb;ipdb.set_trace()
temp=bar.get('response').get('quotes').get('quote')

one=[]
for aDict in temp:
    new=clean(aDict)
    one.append(new)

#bob=DataFrame(one, index=[x.get('datetime') for x in one] )
to_exclude=['datetime', 'date', 'timestamp', 'time', 'vl']

#vl is supposed to be cum vol, but it is wrong ...
#[u'last', u'timestamp', u'time', u'vl', u'datetime', u'date', u'hi', u'lo', u'incr_vl', u'opn']


foo=DataFrame.from_records(one, index='datetime',
    exclude=to_exclude)
#    coerce_float=True)
foo.index=pd.to_datetime(foo.index)

import ipdb;ipdb.set_trace()

aSeries=foo['last']
foo['ewma_20']=ewma(aSeries,span=20,min_periods=20)
foo['ewma_8']=ewma(aSeries,span=8,min_periods=8)



newdata=[]
five=DataFrame()

newDf=foo.resample('5Min', how=ohlc_dict, label='left', closed='left')

print 'bar'
'''

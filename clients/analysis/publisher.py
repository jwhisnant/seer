"""
Set up basic redis publishers and
subscribers

"""
#import ipdb;ipdb.set_trace() #XXX FIX ME
import sys
import json

import txredisapi as redis

from twisted.application import internet
from twisted.application import service
from twisted.python import log

class PublisherProtocol(redis.RedisProtocol):
    """ same old same old ...
    """

class TestingPublisherProtocol(redis.RedisProtocol):
    """ a publishing protocol that sends some test messages
    """
    quote= {'ask': '442.75',
      'asksz': '4',
      'bid': '442.54',
      'bidsz': '1',
      'bidtick': '3',
      'datetime': '2013-05-23T16:15:01-04:00',
      'exch': 'Pacific',
      'qcond': 'Regular, two-sided open quote automated',
      'symbol': 'AAPL',
      'timestamp': '1369340101'}

    trade={'cvol': '12598621',
     'datetime': '2013-05-23T17:13:20-04:00',
     'exch': 'Pacific',
     'last': '442.88',
     'symbol': 'AAPL',
     'tcond': '57,16,10',
     'timestamp': '1369343600',
     'vl': '196',
     'vwap': '442.0288'}


    def connectionMade(self):
        import ipdb;ipdb.set_trace()
        log.msg("Connection made")
        self.sendStockMessages()

    def sendStockMessages(self):
        log.msg("Publishing messages")
        print "publishing messages"
        for aDict in (self.trade,self.quote):
            chan=aDict.get('symbol', 'UNK') + '.'+'quote'
            #self.publish(chan,aDict) # no need to do json
            #self.publish(chan,json.dumps(aDict.items()))

            out={aDict.get('datetime') : aDict.copy()}
            log.msg(out)
            self.publish(chan,json.dumps(out) )

class PublisherFactory(redis.RedisFactory):
    protocol = PublisherProtocol
    def __init__(self, isLazy=False, handler=redis.ConnectionHandler):
        redis.RedisFactory.__init__(self, None, None, 1, isLazy=isLazy,
            handler=handler)

class TestingPublisherFactory(redis.RedisFactory):
    protocol = TestingPublisherProtocol
    def __init__(self, isLazy=False, handler=redis.ConnectionHandler):
        redis.RedisFactory.__init__(self, None, None, 1, isLazy=isLazy,
            handler=handler)

class SubscriberProtocol(redis.SubscriberProtocol):

    def connectionMade(self):
        print "waiting for messages..."
        print "use the redis client to send messages:"
        print "$ redis-cli publish zz test"
        print "$ redis-cli publish foo.bar hello world"
        self.subscribe("zz")
        self.psubscribe("foo.*")
        #reactor.callLater(10, self.unsubscribe, "zz")
        #reactor.callLater(15, self.punsubscribe, "foo.*")

        # self.continueTrying = False
        # self.transport.loseConnection()

    def messageReceived(self, pattern, channel, message):
        print "pattern=%s, channel=%s message=%s" % (pattern, channel, message)

    def connectionLost(self, reason):
        print "lost connection:", reason

class PublisherFactory(PublisherFactory):
    # PublisherFactory is a wapper for the ReconnectingClientFactory
    maxDelay = 120
    continueTrying = True
    protocol = PublisherProtocol

application = service.Application("Stock Testing Publisher")

#srv = internet.TCPClient("127.0.0.1", 6379, PublisherFactory())

# publish some random stock data...
srv = internet.TCPClient("127.0.0.1", 6380, TestingPublisherFactory())
#srv = internet.TCPClient("127.0.0.1", 6379, SubscriberProtocolFactory)

srv.setServiceParent(application)



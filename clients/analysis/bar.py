import ipdb;ipdb.set_trace()
import json
#import pandasjson
from pandas import DataFrame
import pandas as pd
from pandas.stats.moments import ewma

fn='/home/james/Seer/seer/clients/quotes/data/foo.json'


ohlc_dict = {
    'opn':'first',
    'hi':'max',
    'lo':'min',
    'last': 'last',
    'incr_vl': 'sum',
}

def clean(aDict):
    out={}
    for key,value in aDict.iteritems():
        try:
            value=float(value)
        except:
            value=value
        out[key]=value
    return out

bar=json.load(open(fn, 'r'))

#import ipdb;ipdb.set_trace()
temp=bar.get('response').get('quotes').get('quote')

one=[]
for aDict in temp:
    new=clean(aDict)
    one.append(new)

#bob=DataFrame(one, index=[x.get('datetime') for x in one] )
to_exclude=['datetime', 'date', 'timestamp', 'time', 'vl']

#vl is supposed to be cum vol, but it is wrong ...
#[u'last', u'timestamp', u'time', u'vl', u'datetime', u'date', u'hi', u'lo', u'incr_vl', u'opn']


foo=DataFrame.from_records(one, index='datetime',
    exclude=to_exclude)
#    coerce_float=True)
foo.index=pd.to_datetime(foo.index)

import ipdb;ipdb.set_trace()

aSeries=foo['last']
foo['ewma_20']=ewma(aSeries,span=20,min_periods=20)
foo['ewma_8']=ewma(aSeries,span=8,min_periods=8)



newdata=[]
five=DataFrame()

newDf=foo.resample('5Min', how=ohlc_dict, label='left', closed='left')

print 'bar'


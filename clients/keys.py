# logging
import logging
logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

try:
    import private_keys as keys
except ImportError:
    #get your own!
    import example_keys as keys
    logger.warning('Using example keys, you should get your own!')

consumer_key = keys.consumer_key
consumer_secret = keys.consumer_secret
access_token = keys.access_token
access_secret = keys.access_secret

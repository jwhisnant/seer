"""
a class that helps manage messages
for things that want to talk

json is spoken here ...

"""
import os
import uuid
import json

from pprint import pprint as pp

class Message:

    def __init__(self, name='Unknown Application', subscriptions=None):
        """
        """
        self.name=name
        self.pid=os.getpid()
        self.uuid=uuid.uuid4()

        # get identifier first, we need it for these later things ...
        self.identifier='%s.%s.%s'%(self.name,self.pid,self.uuid)

        self.subscriptions=subscriptions
        if not self.subscriptions:
            self.subscriptions=[]

        # everybody gets these
        default_subs=self._default_subscriptions()
        self.subscriptions.extend(default_subs)

        self.pid=os.getpid()


    def send_control_message(self, me='', target='*', msg='control', method='status', payload=None):
        """
        a control messsge is targeted to only one target
        and is designed to have it call a method with the
        given arguments ....
        payload should be **kwargs equivalent
        """
        if not payload:
            payload={'foo':'bar', 'baz':'boo'}

        channel=self.create_outgoing_key(me,target,msg+'.'+method)
        value=json.dumps(payload)

        return channel, value

    def status(self,**kwargs):
        kwargs['received']=self.identifier
        return kwargs

    def _default_subscriptions(self):
        # listen to pings
        inping=self.create_incoming_key(msg='ping')
        # listen to somebody trying to control me ... you know what you doing
        incontrol=self.create_incoming_key(msg='control')
        # listen to blast-ograms
        inadmin=self.create_incoming_key(msg='admin')
        # listen to emergencies
        admin=self.create_incoming_key(msg='emergency', me='*')

        return [inping,incontrol,inadmin, admin]

    def __call__(self):
        """
        """
        """create the key for redis for channel for publication
        format is to|from (which is me as arg)
        you can use '*' for wildcard
        """

    def create_incoming_key(self, me='', target='*', msg='*'):
        """returns a key for most cases or None
        """
        out=None
        if not me:
            me=self.identifier
        out='%s|%s|%s'%(msg,me,target)
        return out

    def create_outgoing_key(self, me='', target='*', msg='*'):
        """returns a key for most cases or None
        """
        out=None
        if not me:
            me=self.identifier
        out='%s|%s|%s'%(msg,target,me)
        return out

    def pong(self, me='', target='*', msg='pong'):
        """pong is the only appropriate ping response ...
        we should have a target
        return channel, msg
        """
        return self.create_outgoing_key(me,target,msg) , json.dumps(msg)


    def ping(self, me='', target='*', msg='ping'):
        """ send ping message
        """
        return self.create_outgoing_key(me,target,msg), json.dumps(msg)

    def decode_control_message(self,channel,message):
        """ decode a control message and return the
        value that is requested ...
        """

        # determine from
        me,src,temp=channel.split('|')
        msg_type,method=temp.split('.')


        out={'msg_type':msg_type,
            'src':src,
            'me':me,
            'method':method,
            'kwargs':message,
            }

        return out


        to_call=getattr(self,method, None)

        # unjsonify it
        kwargs=json.loads(value)

        if to_call:
            result=to_call(**kwargs)

        if not to_call:
            result=None

        out={'msg_type':msg_type,
            'src':src,
            'me':me,
            'method':method,
            'result':result,
            }

        return out



if __name__=='__main__':
    mess=Message('bob')
    print 'ping', mess.ping()
    print 'pong', mess.pong()
    key,value=mess.send_control_message()
    result=mess.call_method_from_remote(key,value)



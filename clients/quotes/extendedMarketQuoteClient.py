from __future__ import print_function
"""
This is a client that does extended market quotes
all of this stuff needs to be refactored
"""

from twisted.internet.task import react
from twisted.internet import reactor
from twisted.web.http_headers import Headers

from keys import consumer_key
from keys import consumer_secret
from keys import access_secret
from keys import access_token

from pprint import pprint as pp
import treq
import oauth2

from twisted.words.xish.xmlstream import XmlStream

from twisted.python import failure

from twisted.internet import protocol
from twisted.protocols import basic

#from thissux import ThisSux
from thissux import StreamingQuotePublisher
from thissux import StreamingQuotesSux

from twisted.web import sux


# some way to send data to other clients ...
from fidreader import FidReader

consumer = oauth2.Consumer(consumer_key, consumer_secret)
token = oauth2.Token(access_token,access_secret )
defaultSignature = oauth2.SignatureMethod_HMAC_SHA1()
headers = Headers()


#https://github.com/habnabit/theresa-bot/blob/master/twatter.py
def oauth_headers(consumer,token,method,uri,parameters=None):
    signatureMethod=defaultSignature
    req = oauth2.Request.from_consumer_and_token(
        consumer, token=token,
        http_method=method, http_url=uri, parameters=parameters,is_form_encoded=True)
    req.sign_request(signatureMethod, consumer, token)
    for header, value in req.to_header().iteritems():
        # oauth2, for some bozotic reason, gives unicode header values
        cleaned=value.encode()
        #cleaned.replace('OAuth realm=""', 'Oauth')
        headers.addRawHeader(header, cleaned)

    return headers


def print_response(response):
    print(response.code, response.phrase)
    print(response.headers)
    return treq.text_content(response).addCallback(print)


fReader=FidReader('/home/james/Seer/seer/clients/quotes/fids.txt')
params=fReader()
if params:
    url_piece='&fids='+','.join(params)
if not params:
    url_piece=''



# non streaming things
site='https://api.tradeking.com'
#resource='/v1/market/clock.json' # this does not require authorization
#resource='/v1/market/clock.xml' # this does not require authorization
resource='/v1/market/ext/quotes.xml?symbols=AAPL,QQQ,MSFT,IBM%s'%(url_piece) # but this does ...
print (resource)

def main(reactor, *args):
    headers=oauth_headers(consumer,token,'GET', uri=site+resource)
    d = treq.get(site+resource, headers=headers)
    d.addCallback(print_response)
    return d

def parse_xml(reactor, url, headers,destination):
    ad = treq.get(url, headers=headers)
    ad.addCallback(treq.collect, destination.dataReceived)
    ad.addBoth(lambda _: destination.connectionLost(failure.Failure()))
    return ad

def cbResponse(ignored):
    print ('Response received')

def cbShutdown(ignored):
    reactor.stop()


headers=oauth_headers(consumer,token,'GET', uri=site+resource)
url=site+resource

#destination=sux.XMLParser()
#destination=StreamingQuotesSux()
destination=StreamingQuotePublisher()
destination.connectionMade() # do some required setup

#destination.addRedisConnection()

ad=parse_xml(reactor, url, headers, destination)
ad.addCallback(cbResponse)
#ad.addBoth(cbShutdown)

def runEverySecond():
    ad=parse_xml(reactor, url, headers, destination)
    ad.addCallback(cbResponse)
    #ad.addBoth(cbShutdown)

from twisted.internet import task
loop = task.LoopingCall(runEverySecond)
loop.start(5.0) # call every second

reactor.run()

#from __future__ import print_function
"""
A custom xml parser that parser stock quotes
"""

import json
import sys
from datetime import datetime

sys.path.append('/home/james/Seer/seer/clients/analysis')
#from publisher import PublisherProtocol
#from publisher import PublisherFactory
#from publisher import myFactory

import txredisapi as redis

from twisted.application import internet
#from twisted.application import service
from twisted.internet.task import react
from twisted.internet import defer

from twisted.python import log
log.startLogging(sys.stdout)

from pprint import pprint as pp
from twisted.web import sux
import time


REDIS_HOST='localhost'
REDIS_PORT=6379

# some way to send data to other clients ...
class StreamingQuotesSux(sux.XMLParser):
    """This in an XMLParser, but we need to over-ride
    some things to be helpful ...
    """

    trades=[]
    quotes=[]

    tag_now=''
    the_data=''

    myOutput=None

    count={'trade':0, 'quote':0}
    excludes=[]

    def tagStruct(self,name):
        if name=='quote':
            self.myOutput=self.quotes
            self.addtoDicts(name)

        if name=='trade':
            self.myOutput=self.trades
            self.addtoDicts(name)

    def addtoDicts(self,name):
        #pp(self.count)
        self.count[name]=self.count[name]+1
        if self.count[name] % 2 == 0:
#            self.#the_dicts.append(dict(self.myOutput))
#            pp(dict(self.myOutput))

            aDict=dict(self.myOutput)
            #channel='%s.quote'%(aDict.get('symbol', 'UNK') )
            channel='%s.%s'%(aDict.get('symbol', 'UNK'), name)
            print channel
            out=self.cleanUpDict(aDict)
            self.publishToRedis(channel, out)
            self.myOutput=[]

    def cleanUpDict(self,aDict):
        """
        return a dictionary that is redis friendly
        for a dataframe to deal with ...
        """
        key=None
        now=aDict.get('datetime')
        aCopy=aDict.copy()
        if now:
            key=now
            del aCopy['datetime']
        if not key:
            key=datetime.now().isoformat()

        out={key : aCopy}
        #log.msg(out)
        return out


#    def addRedisConnection(self, redis_host, redis_port, reconnect=True):
#        raise NotImplementedError

    def publishToRedis(self, channel, aDict):
        raise NotImplementedError

    '''
    @defer.inlineCallbacks
    def xxxpublishToRedis(self, aDict):
        key=None
        now=aDict.get('datetime')
        aCopy=aDict.copy()
        if now:
            key=now
            del aCopy['datetime']
        if not key:
            key=datetime.now().isoformat()

        out={key : aCopy}
        log.msg(out)
        chan='%s.quote'%(aDict.get('symbol', 'UNK') )

        yield self.db.publish(chan, json.dumps(out))
        yield self.db.disconnect()
    '''

    def addAndReset(self):
        #if self.tag_now in self.excludes:
        #    print 'ignoring', self.tag_now

        if self.tag_now not in self.excludes:
            if self.tag_now and self.the_data:
                if self.myOutput is not None:
                    self.myOutput.append((self.tag_now, self.the_data))

        self.tag_now=''
        self.the_data=''

    def gotTagStart(self, name, attributes):
        """Encountered an opening tag.
        Default behaviour is to print."""
        #print ('begin', name, attributes)
        self.tagStruct(name)
        self.tag_now=name

    def gotText(self, data):
        """Encountered text
        Default behaviour is to print."""
        #print ('text:', repr(data))
        self.the_data=data
        self.addAndReset()

    def gotEntityReference(self, entityRef):
        """Encountered mnemonic entity reference
        Default behaviour is to print."""
        pass

    def gotComment(self, comment):
        """Encountered comment.
        Default behaviour is to ignore."""
        pass

    def gotCData(self, cdata):
        """Encountered CDATA
        Default behaviour is to call the gotText method"""
        pass

    def gotDoctype(self, doctype):
        """Encountered DOCTYPE
        This is really grotty: it basically just gives you everything between
        '<!DOCTYPE' and '>' as an argument.
        """
        pass

    def gotTagEnd(self, name):
        """Encountered closing tag
        Default behaviour is to print."""
        pass

class StreamingQuotePublisher(StreamingQuotesSux):
    # set excludes
    excludes=('qcond', 'exch', 'timestamp', )

    @defer.inlineCallbacks
    def publishToRedis(self, channel, out):
        db = yield redis.Connection(REDIS_HOST, REDIS_PORT, reconnect=False)
        yield db.publish(channel, json.dumps(out))
        pp(out)
        yield db.disconnect()

    def tardpublishToRedis(self, channel, out):
        self.db.addCallback(redis.publish , [channel, json.dumps(out)])


class ExtendedMarketQuotePublisher(StreamingQuotesSux):
    """I handle extended market quotes
    right now nothing different than the parent class needed
    """


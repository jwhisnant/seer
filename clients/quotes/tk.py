from __future__ import print_function

#import ipdb;ipdb.set_trace()

from twisted.internet.task import react
from twisted.web.http_headers import Headers

from keys import consumer_key
from keys import consumer_secret
from keys import access_secret
from keys import access_token

from pprint import pprint as pp
import treq
import oauth2

consumer = oauth2.Consumer(consumer_key, consumer_secret)
token = oauth2.Token(access_token,access_secret )

defaultSignature = oauth2.SignatureMethod_HMAC_SHA1()

headers = Headers()

#https://github.com/habnabit/theresa-bot/blob/master/twatter.py
def oauth_headers(consumer,token,method,uri,parameters=None):
    signatureMethod=defaultSignature
    req = oauth2.Request.from_consumer_and_token(
        consumer, token=token,
        http_method=method, http_url=uri, parameters=parameters,is_form_encoded=True)
    req.sign_request(signatureMethod, consumer, token)
    for header, value in req.to_header().iteritems():
        # oauth2, for some bozotic reason, gives unicode header values
        cleaned=value.encode()
        #cleaned.replace('OAuth realm=""', 'Oauth')
        headers.addRawHeader(header, cleaned)

    return headers


def print_response(response):
    print(response.code, response.phrase)
    print(response.headers)

    return treq.text_content(response).addCallback(print)


site='https://api.tradeking.com'
resource='/v1/market/clock' # this does not require authorization
#resource='/v1/market/ext/quotes.json?symbols=AAPL,QQQ,MSFT,AAPL' # but this does ...
# stream attempt
#site='https://stream.tradeking.com'
#resource='/v1/market/quotes?symbols=AAPL,QQQ,MSFT,AAPL' # but this does

def main(reactor, *args):

    headers=oauth_headers(consumer,token,'GET', uri=site+resource)
    d = treq.get(site+resource, headers=headers)
    d.addCallback(print_response)
    return d

react(main, [])
'''
pip freeze
Twisted==13.0.0
argparse==1.2.1
httplib2==0.8
ipdb==0.7
ipython==0.13.2
oauth2==1.5.211
pyOpenSSL==0.13
treq==0.1.0
wsgiref==0.1.2
zope.interface==4.0.5
'''

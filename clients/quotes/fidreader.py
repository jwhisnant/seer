class FidReader:

    def __init__(self, fn, out=None):
        self.fn=fn
        self.out=out

        if not self.out:
            self.out=[]

    def __call__(self):
        for line in open(self.fn):
            if not line.startswith('#'):
                self.out.append(line.strip())
        return self.out

if __name__=='__main__':
    reader=FidReader('/home/james/Seer/seer/clients/quotes/fids.txt')
    bob=reader()
    print bob


from __future__ import print_function
"""
create a class that tries to use the streaming
quote service from trade king
"""

from twisted.internet.task import react
from twisted.internet import reactor
from twisted.words.xish.xmlstream import XmlStream
from twisted.python import failure
from twisted.internet import protocol
from twisted.protocols import basic

from twisted.web.http_headers import Headers
from twisted.application.internet import TimerService
from twisted.application import service

import keys
from twisted.internet.task import LoopingCall

'''from keys import consumer_key
from keys import consumer_secret
from keys import access_secret
from keys import access_token
'''


#from twisted.application.internet import TimerService


from pprint import pprint as pp
import treq
import oauth2

#from thissux import ThisSux
from thissux import StreamingQuotePublisher

# some way to send data to other clients ...
from fidreader import FidReader


'''
consumer = oauth2.Consumer(consumer_key, consumer_secret)
token = oauth2.Token(access_token,access_secret )
defaultSignature = oauth2.SignatureMethod_HMAC_SHA1()
headers = Headers()
'''

class StreamingTradeKingClient(object):

    def __init__(self,
                name='TK Streaming Quote Client',
                consumer_key=None,consumer_secret=None,
                access_token=None,access_secret=None,
                site='https://stream.tradeking.com',
                resource='/v1/market/quotes.xml',
                symbols=None,
                fids=None,
                form='xml',
                method='get',
                ):

        self.destination=StreamingQuotePublisher()
        self.destination.connectionMade() # do some required setup

        self.consumer=consumer_key
        self.consumer_secret=consumer_secret
        self.access_token=access_token
        self.access_secret=access_secret

        self.site=site
        self.resource=resource
        self.symbols=symbols
        self.fids=fids
        self.form=form
        self.method=method


        if not self.consumer:
            self.consumer_key=keys.consumer_key

        if not self.consumer_secret:
            self.consumer_secret=keys.consumer_secret

        if not self.access_token:
            self.access_token=keys.access_token

        if not self.access_secret:
            self.access_secret=keys.access_secret

        if not self.symbols:
            self.symbols=['AAPL', 'IBM',]

        if not self.fids:
            fReader=FidReader('/home/james/Seer/seer/clients/quotes/fids.txt')
            self.fids=fReader()

        self.url=self.buildUrl()
        self.headers=self.oauth_headers(method=self.method, uri=self.url)

    def addSymbols(self, symbols):
        """add symbols, recalculate, restart connection
        """
        self.symbols.extend(symbols)
        self.symbols=sorted(list(set(symbols)))

        self.url=self.buildUrl()
        self.headers=self.oauth_headers(method=self.method, uri=self.url)


    def restart(self):
        """something has changed in url, so restart process ...
        """


    def removeSymbols(self,symbols):
        """remove symbols, recalculate, restart connection
        """
        for rem in symbols:
            if rem in self.symbols:
                self.symbols.remove(rem)

        self.url=self.buildUrl()
        self.headers=self.oauth_headers(method=self.method, uri=self.url)

    def buildUrl(self):
        """
        """
        # build url
        symString=','.join(self.symbols)
        fidString=','.join(self.fids)

        #XXX FIX ME
        url='%s/%s.%s?symbols=%s' %(self.site,self.resource,self.form,symString)
        if self.fids:
            url=url+'&fids=%s'%(fidString)
        print(url)
        return url

    #https://github.com/habnabit/theresa-bot/blob/master/twatter.py
    def oauth_headers(self,method,uri,parameters=None):
        headers=Headers()
        signatureMethod=oauth2.SignatureMethod_HMAC_SHA1()

        consumer = oauth2.Consumer(self.consumer_key, self.consumer_secret)
        token = oauth2.Token(self.access_token,self.access_secret )


        #signatureMethod=defaultSignature
        req = oauth2.Request.from_consumer_and_token(
            consumer, token=token,
            http_method=method, http_url=uri, parameters=parameters,is_form_encoded=True)
        req.sign_request(signatureMethod, consumer, token)
        for header, value in req.to_header().iteritems():
            # oauth2, for some bozotic reason, gives unicode header values
            cleaned=value.encode()
            #cleaned.replace('OAuth realm=""', 'Oauth')
            headers.addRawHeader(header, cleaned)

        return headers

    def print_response(self,response):
        print(response.code, response.phrase)
        print(response.headers)
        return treq.text_content(response).addCallback(print)

    def parse_xml(self,reactor):#, url, headers,destination):
        ad = treq.get(self.url, headers=self.headers)
        ad.addCallback(treq.collect, self.destination.dataReceived)
        return ad


def cbResponse(ignored):
    print ('Response received')

def cbShutdown(ignored):
    reactor.stop()



client=StreamingTradeKingClient()

ad=client.parse_xml(reactor)
ad.addCallback(cbResponse)

#ad.addBoth(cbShutdown)

#lc2 = LoopingCall(tired_task)
#lc2.start(0.5)

reactor.run()

'''
interval=60*60*24 # each day restart
import ipdb;ipdb.set_trace()
application = service.Application('TkStreaming')
#streamService = TimerService(interval, client.parsexml, **kwargs)
streamService = TimerService(interval, client.parse_xml)
streamService.setServiceParent(application)

#reactor.run()
'''

'''
fReader=FidReader('/home/james/Seer/seer/clients/quotes/fids.txt')
params=fReader()

if params:
    url_piece='&fids='+','.join(params)
if not params:
    url_piece=''

# stream attempt
site='https://stream.tradeking.com'
resource='/v1/market/quotes.xml?symbols=AAPL,QQQ,MSFT,IBM' # but this does

def main(reactor, *args):
    headers=oauth_headers(consumer,token,'GET', uri=site+resource)
    d = treq.get(site+resource, headers=headers)
    d.addCallback(print_response)
    return d
'''




'''
headers=oauth_headers(consumer,token,'GET', uri=site+resource)
url=site+resource

#react(download_file, [url, headers,'download3.txt'])
#react(parse_xml, [url, headers,'None'])

#ad=download_file(reactor, url, None, 'download3.txt')

destination=StreamingQuotePublisher()
#destination=ThisSux()
destination.connectionMade() # do some required setup
#destination.publishToRedis()

ad=parse_xml(reactor, url, headers, destination)
ad.addCallback(cbResponse)
#ad.addBoth(cbShutdown)

reactor.run()
'''

from __future__ import print_function

# logging
import logging
logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from twisted.internet.task import react
from twisted.web.http_headers import Headers

from keys import consumer_key
from keys import consumer_secret
from keys import access_secret
from keys import access_token

from pprint import pprint as pp
import treq
import oauth2

from twisted.internet.defer import Deferred, succeed
from twisted.internet.protocol import Protocol
from twisted.web.client import ResponseDone



from twisted.words.xish.xmlstream import XmlStream

consumer = oauth2.Consumer(consumer_key, consumer_secret)
token = oauth2.Token(access_token,access_secret )

defaultSignature = oauth2.SignatureMethod_HMAC_SHA1()

headers = Headers()

#https://github.com/habnabit/theresa-bot/blob/master/twatter.py
def oauth_headers(consumer,token,method,uri,parameters=None):
    signatureMethod=defaultSignature
    req = oauth2.Request.from_consumer_and_token(
        consumer, token=token,
        http_method=method, http_url=uri, parameters=parameters,is_form_encoded=True)
    req.sign_request(signatureMethod, consumer, token)
    for header, value in req.to_header().iteritems():
        # oauth2, for some bozotic reason, gives unicode header values
        cleaned=value.encode()
        #cleaned.replace('OAuth realm=""', 'Oauth')
        headers.addRawHeader(header, cleaned)

    return headers



site='https://api.tradeking.com'
#resource='/v1/market/clock' # this does not require authorization
#resource='/v1/market/timesales.json?symbols=AAPL&startdate=2013-05-07&enddate=2013-05-10&interval=1min'
#resource='/v1/market/timesales.json?symbols=AAPL&startdate=2013-05-07&enddate=2013-05-10&interval=1min'
#resource='/v1/market/timesales.json?symbols=AAPL&startdate=2013-05-07&enddate=2013-05-10&interval=5min'
#resource='/v1/market/timesales.?symbols=AAPL&startdate=2013-05-13&interval=1min'
resource='/v1/market/timesales.xml?symbols=AAPL&startdate=2013-05-13&interval=1min'

def download_file(reactor, destination_filename):
    destination = file(destination_filename, 'w', 0)
    headers=oauth_headers(consumer,token,'GET', uri=site+resource)
    ad = treq.get(site+resource, headers=headers)

    ad.addCallback(treq.collect, destination.write)
    ad.addBoth(lambda _: destination.close())
    return ad

fns=['/tmp/timesales.xml']
react(download_file, fns)



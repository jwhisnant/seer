#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
File: stream_json.py
Author: James Whisnant
Email: jwhisnant@gmail.com
Github: https://github.com/jwhisnant
Description: 
    Toy to try to do incremental
json parsing - the tl;dr is it is even
worse than trying to do expat, so do that instead
"""

from __future__ import print_function

from twisted.internet.task import react
from twisted.web.http_headers import Headers

from keys import consumer_key
from keys import consumer_secret
from keys import access_secret
from keys import access_token

from pprint import pprint as pp
import treq
import oauth2
import time

consumer = oauth2.Consumer(consumer_key, consumer_secret)
token = oauth2.Token(access_token,access_secret )

defaultSignature = oauth2.SignatureMethod_HMAC_SHA1()

headers = Headers()

def oauth_headers(consumer,token,method,uri,parameters=None):
    signatureMethod=defaultSignature
    req = oauth2.Request.from_consumer_and_token(
        consumer, token=token,
        http_method=method, http_url=uri, parameters=parameters,is_form_encoded=True)
    req.sign_request(signatureMethod, consumer, token)
    for header, value in req.to_header().iteritems():
        # oauth2, for some bozotic reason, gives unicode header values
        cleaned=value.encode()
        #cleaned.replace('OAuth realm=""', 'Oauth')
        headers.addRawHeader(header, cleaned)

    return headers


def print_response(response):
    print(response.code, response.phrase)
    print(response.headers)
    #print(response.body)
    return treq.text_content(response).addCallback(print)


site='https://api.tradeking.com'
#resource='/v1/market/clock' # this does not require authorization
resource='/v1/market/ext/quotes.json?symbols=AAPL,QQQ,MSFT,IBM' # but this does ...

# stream attempt
site='https://stream.tradeking.com'
resource='/v1/market/quotes.json?symbols=AAPL,QQQ,MSFT,IBM,GOOG,BAC' # but this does

def main(reactor, *args):
    headers=oauth_headers(consumer,token,'GET', uri=site+resource)
    d = treq.get(site+resource, headers=headers)
    d.addCallback(print_response)
    return d

#react(main, [])

# try to stream ...
def download_file(reactor, destination_filename=''):
    destination = file(destination_filename, 'w', 0)

    headers=oauth_headers(consumer,token,'GET', uri=site+resource)
    d = treq.get(site+resource, headers=headers)

#    d.addCallback(print_response)
#    d = treq.get(url)
    d.addCallback(treq.collect, destination.write)
    d.addBoth(lambda _: destination.close())
    return d

react(download_file, ['/data/download5.json'])

## Synopsis

The is part of a pet project to beat the stock market. Here are some examples of interacting with TradeKing's API to gather stock maket data.
The goal of this portion is to use twisted to either gather data at points in time, or via the streaming API using treq.
Data will be stored in redis for publication and subscription for various worker processes.
A timer process is planned for doing resampling of pandas dataframes over time.
Not yet well implemented are gathering html and rss data.

## Code Example

Show what the library does as concisely as possible, developers should be able to figure out **how** your project solves their problem by looking at the code example. Make sure the API you are showing off is obvious, and that your code is short and concise.

## Motivation

A short description of the motivation behind the creation and maintenance of the project. This should explain **why** the project exists.

## Prerequistites

There are a number of prerequisites.

[redis] (http://redis.io/)

[pandas] (http://pandas.pydata.org/)

[twisted] (https://twistedmatrix.com/trac/)

## Installation

Using a virtualenv is recommended.
pip install -r requirements.txt

## API Reference

Depending on the size of the project, if it is small and simple enough the reference docs can be added to the README. For medium size to larger projects it is important to at least provide a link to where the API reference docs live.

## Tests

Describe and show how to run the tests with code examples.

## Contributors

Let people know how they can dive into the project, include important links to things like issue trackers, irc, twitter accounts if applicable.

## License

A short snippet describing the license (MIT, Apache, etc.)

